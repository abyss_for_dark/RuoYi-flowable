import request from '@/utils/request'

// 登录方法
export function login(username, password, code, uuid) {
  const data = {
    username,
    password,
    code,
    uuid
  }
  return request({
    url: '/login',
    headers: {
      isToken: false,
      repeatSubmit: false
    },
    method: 'post',
    data: data
  })
}

// 注册方法
export function register(data) {
  return request({
    url: '/register',
    headers: {
      isToken: false
    },
    method: 'post',
    data: data
  })
}

// 获取用户详细信息
export function getInfo() {
  return request({
    url: '/getInfo',
    method: 'get'
  })
}

// 退出方法
export function logout() {
  return request({
    url: '/logout',
    method: 'post'
  })
}

// 获取验证码
export function getCodeImg() {
  return request({
    url: '/captchaImage',
    headers: {
      isToken: false
    },
    method: 'get',
    timeout: 20000
  })
}

export function getAToken(code) {
  const data = {
    code,
  }
  return request({
    // url: 'http://sso.maxkey.top/sign/authz/oauth/v20/token?client_id=960177872293593088&client_secret=AxoQMjgwMjIwMjQxMTI1NTE3MzcHCc&grant_type=authorization_code&redirect_uri=http://localhost:8090/authorize&code='+code,
    url: '/ssoToken',
    method: 'post',
    data: data
  })
}
